export default function(state, action) {
  return [
    { title: 'Javascript: The Good Parts', pages: 176 },
    { title: "Harry Potter and the Sorcerer's Stone", pages: 309 },
    { title: 'The Dark Tower I: The Gunslinger', pages: 288 },
    { title: 'Eloquent Ruby', pages: 448 }
  ]
}